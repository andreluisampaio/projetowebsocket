﻿using ExcelDna.Integration.Rtd;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using System.Xml;
using Microsoft.Extensions.Logging;

namespace RTD.Excel
{
    [ComVisible(true)]
    public class SignalRServer2 : IRtdServer
    {
        private Dictionary<int, Topic> _topics;
        private IRTDUpdateEvent _callback;
        private System.Windows.Forms.Timer _timer;

        public const string ServerProgId = "ExcelDNARTD2.SignalRServer2";
        private HubConnection _connection;

        public SignalRServer2()
        {
            //http://localhost:49173/notificacao

            _connection = new HubConnectionBuilder()
                //.WithUrl("https://localhost:44373/notificacao")
                .WithUrl("https://signalrtests20220315.azurewebsites.net/chat")
                //.WithUrl($"https://pricerbridge-dev.cloud.itau.com.br/bridge/hubs/v1/consumer?guid={Guid.NewGuid().ToString()}", opt =>
                //{
                //    opt.Transports = Microsoft.AspNetCore.Http.Connections.HttpTransportType.WebSockets;
                //    opt.SkipNegotiation = true;
                //})
                .WithAutomaticReconnect()
                .ConfigureLogging(logging =>
                {
                    // Log to the Console
                    logging.AddDebug();

                    logging.SetMinimumLevel(LogLevel.Information);
                    logging.AddFilter("Microsoft.AspNetCore.SignalR", LogLevel.Debug);
                    logging.AddFilter("Microsoft.AspNetCore.Http.Connections", LogLevel.Debug);
                })
                .Build();

            _connection.StartAsync();

            _connection.On<WeatherForecast>("addMessage2", (sensor2) =>
            {
                lock (_topics)
                {
                    try
                    {
                        foreach (KeyValuePair<int, Topic> x in _topics)
                        {
                            GetDataFromXML(x.Value, sensor2);
                        }
                    }
                    catch (Exception)
                    {
                    }
                }

                _callback.UpdateNotify();
            });
        }

        private void GetDataFromXML(Topic topic, WeatherForecast sensor2)
        {
            topic.Value = sensor2.TemperatureC.ToString();
        }

        private void GetDataFromXML(Topic topic, string status)
        {
            topic.Value = status;
        }

        public object ConnectData(int topicId, ref Array strings, ref bool newValues)
        {
            if (strings.Length != 2)
            {
                throw new Exception("Expecting TICKER");
            }

            string filename = strings.GetValue(0) as string;
            if (filename == null || filename.Length == 0)
            {
                throw new Exception("Expecting FILENAME");
            }
            string ticker = strings.GetValue(1) as string;
            if (ticker == null || ticker.Length == 0)
            {
                throw new Exception("Expecting TICKER");
            }

            _topics.Add(topicId, new Topic(filename, ticker));

            return "Queued";
        }

        public int ServerStart(IRTDUpdateEvent CallbackObject)
        {
            _topics = new Dictionary<int, Topic>();
            _callback = CallbackObject;
            _timer = new System.Windows.Forms.Timer();
            _timer.Tick += Callback;
            _timer.Interval = 2000;
            _timer.Start();

            return 1;
        }

        public Array RefreshData(ref int topicCount)
        {
            object[,] results = new object[2, _topics.Count];
            topicCount = 0;

            foreach (int topicID in _topics.Keys)
            {
                if (_topics[topicID].Updated == true)
                {
                    results[0, topicCount] = topicID;
                    results[1, topicCount] = _topics[topicID].Value + " : " + Convert.ToString(DateTime.Now);
                    topicCount++;
                }
            }

            object[,] temp = new object[2, topicCount];
            for (int i = 0; i < topicCount; i++)
            {
                temp[0, i] = results[0, i];
                temp[1, i] = results[1, i];
            }

            return temp;
        }

        public void DisconnectData(int TopicID)
        {
            _timer.Dispose();
            _topics.Remove(TopicID);
        }

        public int Heartbeat()
        {
            return 1;
        }

        public void ServerTerminate()
        {
            _topics = null;
        }

        private void Callback(object sender, EventArgs e)
        {

            lock (_topics)
            {
                try
                {
                    foreach (KeyValuePair<int, Topic> x in _topics)
                    {
                        GetDataFromXML(x.Value, _connection.State.ToString());
                    }
                }
                catch (Exception)
                {
                }
            }

            _callback.UpdateNotify();
        }
    }

    public class Topic
    {
        private string iFileName;
        private string iTicker;
        private string iValue;
        private bool iUpdated;
        public string FileName
        {
            get { return iFileName; }
            set { iFileName = value; }
        }
        public string Ticker
        {
            get { return iTicker; }
            set { iTicker = value; }
        }
        public string Value
        {
            get { return iValue; }
            set
            {
                if (iValue != value)
                {
                    iValue = value;
                    iUpdated = true;
                }
                else
                {
                    iUpdated = false;
                }
            }
        }
        public bool Updated
        {
            get { return iUpdated; }
            set { iUpdated = value; }
        }

        public Topic(string File, string TickerName)
        {
            iFileName = File;
            iTicker = TickerName;
            iValue = "";
        }
    }
}
