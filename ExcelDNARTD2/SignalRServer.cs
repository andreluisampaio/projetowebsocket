﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.SignalR.Client;
using ExcelDna.Integration.Rtd;
using ExcelDna.Integration;
using System.Threading;

namespace RTD.Excel
{
    [ComVisible(true)]                   // Required since the default template puts [assembly:ComVisible(false)] in the AssemblyInfo.cs
    [ProgId(SignalRServer.ServerProgId)]     //  If ProgId is not specified, change the XlCall.RTD call in the wrapper to use namespace + type name (the default ProgId)
    public class SignalRServer : ExcelRtdServer
    {
        public const string ServerProgId = "ExcelDNARTD2.SignalRServer";
        private HubConnection _connection;

        public SignalRServer()
        {
            //http://localhost:49173/notificacao

            _connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:44373/notificacao")
                .WithAutomaticReconnect()
                .ConfigureLogging(logging =>
                {
                    // Log to the Console
                    logging.AddDebug();

                    logging.SetMinimumLevel(LogLevel.Information);
                    logging.AddFilter("Microsoft.AspNetCore.SignalR", LogLevel.Debug);
                    logging.AddFilter("Microsoft.AspNetCore.Http.Connections", LogLevel.Debug);
                })
                .Build();

            _connection.StartAsync();

            //_connection.On<string>("addMessage", (endpointDataId) =>
            //{
            //    foreach (Topic topic in _topics)
            //    {
            //        topic.UpdateValue(endpointDataId);
            //    }
            //});

            _connection.On<WeatherForecast>("addMessage2", (sensor2) =>
            {
                foreach (Topic topic in _topics)
                {
                    topic.UpdateValue(sensor2.TemperatureC);
                }
            });
        }

        List<Topic> _topics;

        protected override bool ServerStart()
        {
            _topics = new List<Topic>();
            return true;
        }

        protected override void ServerTerminate()
        {
        }

        protected override object ConnectData(Topic topic, IList<string> topicInfo, ref bool newValues)
        {
            _topics.Add(topic);
            
            do
            {
                topic.UpdateValue(_connection.State.ToString());
                Thread.Sleep(250);
                
            } while (_connection.State != HubConnectionState.Connected);

            return _connection.State.ToString();
        }

        protected override void DisconnectData(Topic topic)
        {
            _topics.Remove(topic);
        }
    }
}