﻿using ExcelDna.Integration;
using RTD.Excel;
using System;
using System.IO;
using System.Text.RegularExpressions;
using Excel = Microsoft.Office.Interop.Excel;

namespace RTD.Excel1
{
    public static class MyFunctions
    {
        [ExcelFunction(Name = "TesteArrayResizer2")]
        public static object TesteArrayResizer2(
        [ExcelArgument(Name = "Linhas", AllowReference = true)] int rows,
        [ExcelArgument(Name = "Colunas", AllowReference = true)] int columns,
        [ExcelArgument(Name = "Referencia", AllowReference = true)] object referencia)
        {
            //ExcelReference xlref = new ExcelReference(row, row, col, col, "s2cfg");

            var array = MakeArray(rows, columns);
            var caller = XlCall.Excel(XlCall.xlfCaller) as ExcelReference;

            return AsyncFunctions.ArrayResizer2.Resize(array);
        }

        [ExcelFunction(Name = "GetSignalRMessages")]
        public static object GetSignalRMessages(
        [ExcelArgument(Name = "Endpoint", AllowReference = true)] string endpoint,
        [ExcelArgument(Name = "Referencia", AllowReference = true)] object referencia)
        {
            ////endpoint - gerar um identificador unico para passar na funcao rtd, quando responder, cada rtd responde seu topico
            ////var sensor2 = XlCall.RTD(SignalRServer.ServerProgId, null, endpoint) as WeatherForecast;

            ////var array = ConverterEmArray(sensor2);

            ////ExcelReference excelRef = (ExcelReference)referencia;

            ////return AsyncFunctions.ArrayResizer.ResizeTest(array, excelRef);

            return XlCall.RTD(SignalRServer.ServerProgId, null, endpoint);
        }

        public static object GetSignalRMessages2(string itemPath)
        {
            return XlCall.RTD("RTD.Excel.SignalRServer2", null, _dataPath, itemPath);
        }

        [ExcelFunction(Name = "WhatTimeIsIt")]
        public static object WhatTimeIsIt()
        {
            return XlCall.RTD("MyRTDServers.TimeServer", null, "NOW");
        }

        [ExcelFunction(Name = "WhatTimeIsItEx")]
        public static object WhatTimeIsItEx(string input)
        {
            return XlCall.RTD("MyRTDServers.TimeServer", null, "NOW", input);
        }

        [ExcelFunction(Name = "Test")]
        public static object[,] Test(string name)
        {
            object result = XlCall.RTD("MyRTDServers.TimeServer2", null, "NOW");
            return new object[,] { { result } };
        }

        static string _dataPath;

        static MyFunctions()
        {
            string xllDir = Path.GetDirectoryName((string)XlCall.Excel(XlCall.xlGetName));
            _dataPath = Path.Combine(xllDir, "Test.xml");
        }

        public static object GetTestItem(string itemPath)
        {
            return XlCall.RTD("MyRTDServers.TestServer", null, _dataPath, itemPath);
        }

        public static object GetEurOnd()
        {
            return GetTestItem("EUR/OND");
        }

        private static object[,] ConverterEmArray(WeatherForecast sensor2)
        {
            int rows = 1;
            int columns = 4;
            //object[,] result = new object[rows, columns];

            //for (int i = 0; i < rows; i++)
            //{
            //    result[i, 0] = sensor2.Date;
            //    result[i, 1] = sensor2.TemperatureC;
            //    result[i, 2] = sensor2.TemperatureF;
            //    result[i, 3] = sensor2.Summary;
            //}

            object[,] result = new object[rows, columns];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    result[i, j] = i + j;
                }
            }

            return result;
        }

        public static object[,] MakeArray(int rows, int columns)
        {
            object[,] result = new object[rows, columns];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    result[i, j] = i + j;
                }
            }

            return result;
        }

        //[ExcelFunction(Name = "Teste")]
        //public static string Teste(
        //    [ExcelArgument(Name = "range", AllowReference = true)] object range,
        //    [ExcelArgument(Name = "RTD")] string rtd)
        //{
        //    ExcelReference excelrange = (ExcelReference)range;
        //    string internalSheetName = (string)XlCall.Excel(XlCall.xlSheetNm, excelrange);
        //    Match match = Regex.Match(internalSheetName, @"\[(.*)\](.*)");
        //    string workbookName = match.Groups[1].Value;
        //    string sheetName = match.Groups[2].Value;

        //    ////Excel.Application app = new Excel.Application();
        //    Excel.Application app = (Excel.Application)ExcelDnaUtil.Application;
        //    Excel.Worksheet worksheet = (Excel.Worksheet)app.Workbooks[workbookName].Sheets[sheetName];

        //    //Excel.Range range2 = (Excel.Range)range;
        //    //var sheet = range2.Worksheet;
        //    //var sheetName = sheet.Name;

        //    //var workbook = (Excel.Workbook)sheet.Parent;
        //    //var workbookName = workbook.Name;
        //    //var app = workbook.Parent;        



        //    return app.Name + " " + workbookName + " " + sheetName;
        //}

        ////public static string ObterSheetName(string sheetFullName)
        ////{
        ////    int pos = sheetFullName.IndexOf(']');
        ////    return sheetFullName.Substring(pos + 1, sheetFullName.Length - pos - 1);
        ////}
    }
}
