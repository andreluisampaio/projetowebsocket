﻿using ExcelDna.Integration;
using ExcelDna.Integration.Rtd;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;

namespace MyRTDServers
{
    [ComVisible(true)]
    public class TimeServer : ExcelRtdServer
    {
        string _logPath;
        List<Topic> _topics;
        Timer _timer;
        public TimeServer()
        {
            _logPath = @"C:\Users\andre\source\repos\ProjetoWebSocket\ExcelDNARTD2\ExcelDnaRTD.log";
            _topics = new List<Topic>();
            _timer = new Timer(delegate
            {
                string now = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff");
                foreach (Topic topic in _topics) topic.UpdateValue(now);
            }, null, 0, 2000);
            Log("TimerServer created");
        }

        void Log(string format, params object[] args)
        {
            File.AppendAllText(_logPath, DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff") + " - " + string.Format(format, args) + "\r\n");
        }

        protected override bool ServerStart()
        {
            Log("ServerStart");
            return true;
        }

        protected override void ServerTerminate()
        {
            Log("ServerTerminate");
        }

        protected override object ConnectData(Topic topic, System.Collections.Generic.IList<string> topicInfo, ref bool newValues)
        {
            Log("ConnectData: {0} - {{{1}}}", topic.TopicId, string.Join(", ", topicInfo));
            _topics.Add(topic);
            return ExcelErrorUtil.ToComError(ExcelError.ExcelErrorNA);
        }

        protected override void DisconnectData(Topic topic)
        {
            _topics.Remove(topic);
            Log("DisconnectData: {0}", topic.TopicId);
        }
    }
}
