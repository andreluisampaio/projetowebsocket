﻿using System;
using Microsoft.AspNetCore.SignalR.Client;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using static ExcelDna.Integration.Rtd.ExcelRtdServer;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace ExcelDnaRTDServer
{
    public class Program
    {
        private static List<Topic> _topics = new List<Topic>();

        static async Task Main(string[] args)
        {
            //SignalRServer x = new SignalRServer();

            var cn = new HubConnectionBuilder()
                 .WithUrl("https://localhost:44373/notificacao")
                 .WithAutomaticReconnect()
                 .Build();

            cn.On<string>("addMessage", (endpointDataId) =>
            {
                foreach (Topic topic in _topics)
                {
                    topic.UpdateValue(endpointDataId);
                }
            });

            cn.On<WeatherForecast>("addMessage2", (sensor2) =>
            {
                foreach (Topic topic in _topics)
                {
                    topic.UpdateValue(sensor2.TemperatureC);
                }
            });

            //cn.On<string, string>("ReceiveMessage", (user, message) =>
            //{
            //    var newMessage = $"{user}: {message}";
            //    foreach (Topic topic in _topics)
            //    {
            //        topic.UpdateValue(newMessage);
            //    }
            //});

            await cn.StartAsync();

            var items =
                from sensor in cn.AsObservable<WeatherForecast>("addMessage2")
                //where sensor.Value < 5
                select sensor;

            items.Subscribe(sensor => Console.WriteLine(sensor.TemperatureC));

            Console.WriteLine("Receiving Values. Press any key to exit.");
            Console.ReadLine();
        }       
    }
    
}