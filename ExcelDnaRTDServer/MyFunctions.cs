﻿using ExcelDna.Integration;
namespace ExcelDnaRTDServer
{
    public static class MyFunctions
    {
        [ExcelFunction(Name = "sRTD")]
        public static object GetSignalRMessages(string endpoint)
        {
            //endpoint - gerar um identificador unico para passar na funcao rtd, quando responder, cada rtd responde seu topico
            string endpointDataId = XlCall.RTD(SignalRServer.ServerProgId, null, endpoint).ToString();

            return endpointDataId;
        }

    }
}
