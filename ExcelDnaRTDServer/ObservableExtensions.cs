﻿using System;
using Microsoft.AspNetCore.SignalR.Client;
using System.Reactive.Subjects;
using System.Threading.Tasks;

namespace ExcelDnaRTDServer
{
    public static class ObservableExtensions
    {
        public static IObservable<T> AsObservable<T>(this HubConnection connection, string methodName)
        {
            var subject = new Subject<T>();
            Task.Run(async () =>
            {
                var stream = connection.StreamAsync<T>(methodName);
                await foreach (var value in stream)
                {
                    subject.OnNext(value);
                }
            });
            return subject;
        }
    }
}
