﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;

namespace WebSocketStreaming.Model
{
    public class AssetNotificacao
    {
        public AssetNotificacao(Asset ativo)
        {
            Ativo = ativo;
            Inicial();
        }

        private void Inicial()
        {
            string output = JsonConvert.SerializeObject(this);

            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            byte[] Array;
            bf.Serialize(ms, output);
            Array = ms.ToArray();            

            Tamanho = Array.Length;
            Consultar = false;
        }

        public bool Consultar { get; set; }

        public long Tamanho { get; set; }

        public Asset Ativo { get; set; }
    }
}
