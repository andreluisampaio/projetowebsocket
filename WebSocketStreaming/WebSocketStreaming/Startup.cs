using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
//using Newtonsoft.Json.Serialization;
using WebSocketStreaming.Hubs;
using WebSocketStreaming.Repositories;

namespace WebSocketStreaming
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();

            services.AddScoped<IJsonFilesRepository, JsonFilesRepository>();

            services.AddSignalR(config =>
            {
                config.MaximumReceiveMessageSize = 128 * 1024;
            });
            //.AddNewtonsoftJsonProtocol(options =>
            //{
            //    /* Use PascalCase instead of camelCase */
            //    options.PayloadSerializerSettings.ContractResolver = new DefaultContractResolver();
            //});

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", policy =>
                {
                    policy.WithOrigins("http://localhost:4200")
                    .SetIsOriginAllowedToAllowWildcardSubdomains()
                    .AllowAnyMethod().AllowAnyHeader().AllowCredentials();
                });
            });                      
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseCors("CorsPolicy");

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapHub<UploadHub>("/uploadhub");
                endpoints.MapHub<DownloadHub>("/downloadhub");
            });
        }
    }
}
