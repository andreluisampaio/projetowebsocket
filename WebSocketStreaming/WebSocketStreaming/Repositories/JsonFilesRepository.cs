﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WebSocketStreaming.Repositories
{

    public class JsonFilesRepository : IJsonFilesRepository
    {
        private const string Root = "Files/";
        public Dictionary<string, string> Files { get; } = new Dictionary<string, string>();

        public JsonFilesRepository()
        {

        }

        public void GetFiles(params string[] files)
        {
            var filesList = files.ToList();
            if (!filesList.Any())
                foreach (var file in Directory.GetFiles(Root))
                    filesList.Add(Path.GetFileName(file));

            foreach (var file in filesList)
            {
                var path = Path.Combine(Root, file);
                var contents = File.ReadAllText(path);
                Files.Add(file, contents);
            }
        }
    }
}