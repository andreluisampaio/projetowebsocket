﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSocketStreaming.Repositories
{
    public interface IJsonFilesRepository
    {
        Dictionary<string, string> Files { get; }

        void GetFiles(params string[] files);
    }
}
