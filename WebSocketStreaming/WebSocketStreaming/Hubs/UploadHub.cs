﻿using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Channels;
using System.Threading.Tasks;
using WebSocketStreaming.Model;

namespace WebSocketStreaming.Hubs
{
    public class UploadHub : Hub
    {



        public async Task UploadStream(ChannelReader<string> stream)
        {
            var notificacao = string.Empty;
            while (await stream.WaitToReadAsync())
            {
                while (stream.TryRead(out var notificacaoParte))
                {
                    notificacao += Helper.DecodeBase64(System.Text.Encoding.ASCII, notificacaoParte.ToString());
                }
            }

            AssetNotificacao assetNotificacao =
                    JsonConvert.DeserializeObject<Dictionary<string, AssetNotificacao>>(notificacao).ElementAt(0).Value;

            
        }


    }
}
