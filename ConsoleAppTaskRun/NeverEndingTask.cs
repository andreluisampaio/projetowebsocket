﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleAppTaskRun
{
    /// <summary>
    /// - Inherit from NeverEndingTask and override the ExecutionCore method with the work you want to do.
    /// - Changing ExecutionLoopDelayMs allows you to adjust the time between loops e.g. if you wanted to use a backoff algorithm.
    /// - Start/Stop provide a synchronous interface to start/stop task.
    /// - LongRunning means you will get one dedicated thread per NeverEndingTask.
    /// - This class does not allocate memory in a loop unlike the ActionBlock based solution above.
    /// </summary>
    public abstract class NeverEndingTask
    {
        // Using a CTS allows NeverEndingTask to "cancel itself"
        private readonly CancellationTokenSource _cts = new CancellationTokenSource();

        protected NeverEndingTask()
        {
            TheNeverEndingTask = new Task(
               () =>
               {
                // Wait to see if we get cancelled...
                while (!_cts.Token.WaitHandle.WaitOne(ExecutionLoopDelayMs))
                   {
                    // Otherwise execute our code...
                    ExecutionCore(_cts.Token);
                   }
                // If we were cancelled, use the idiomatic way to terminate task
                _cts.Token.ThrowIfCancellationRequested();
               },
               _cts.Token,
               TaskCreationOptions.DenyChildAttach | TaskCreationOptions.LongRunning);

            // Do not forget to observe faulted tasks - for NeverEndingTask faults are probably never desirable
            TheNeverEndingTask.ContinueWith(x =>
            {
                Trace.TraceError(x.Exception.InnerException.Message);
                // Log/Fire Events etc.
            }, TaskContinuationOptions.OnlyOnFaulted);

        }

        protected readonly int ExecutionLoopDelayMs = 1000;
        protected Task TheNeverEndingTask;

        public void Start()
        {
            // Should throw if you try to start twice...
            TheNeverEndingTask.Start();
        }

        protected abstract void ExecutionCore(CancellationToken cancellationToken);

        public void Stop()
        {
            // This code should be reentrant...
            _cts.Cancel();
            TheNeverEndingTask.Wait();
        }
    }
}
