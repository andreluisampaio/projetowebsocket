﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace ConsoleAppTaskRun
{
    class Program
    {
        private static CancellationTokenSource wtoken = new CancellationTokenSource();
        private static Task task;
        private static Random rdn = new Random();

        static void Main(string[] args)
        {
            #region exemplo1

            //Servico servico = new Servico();
            //servico.Start();
            //Console.ReadKey();

            #endregion

            #region exemplo2

            //40 min de 11 para 13
            StartWork();
            Console.ReadKey();

            #endregion

            #region exemplo3

            //task = Task.Factory.StartNew((a) =>
            //{
            //    while (true)
            //    {
            //        wtoken.Token.ThrowIfCancellationRequested();
            //        DoWork();
            //        Thread.Sleep(1000);
            //    }
            //}, wtoken, TaskCreationOptions.LongRunning);

            //task.Wait();

            #endregion
        }

        private static void DoWork()
        {
            Console.WriteLine("Teste " + rdn.Next());
        }

        static void StartWork()
        {
            // Create the token source.
            wtoken = new CancellationTokenSource();

            // Set the task.
            ITargetBlock<DateTimeOffset> task1 = CreateNeverEndingTask((now, ct) => DoWorkAsync(ct), wtoken.Token);

            // Start the task.  Post the time.
            task1.Post(DateTimeOffset.Now);
        }

        private static Task DoWorkAsync(CancellationToken ct)
        {
            Console.WriteLine("Teste " + rdn.Next());
            return Task.CompletedTask;
        }

        static ITargetBlock<DateTimeOffset> CreateNeverEndingTask(
            Func<DateTimeOffset, CancellationToken, Task> action,
            CancellationToken cancellationToken)
        {
            // Validate parameters.
            if (action == null) throw new ArgumentNullException("action");

            // Declare the block variable, it needs to be captured.
            ActionBlock<DateTimeOffset> block = null;

            // Create the block, it will call itself, so
            // you need to separate the declaration and
            // the assignment.
            // Async so you can wait easily when the
            // delay comes.
            block = new ActionBlock<DateTimeOffset>(async now => {
                // Perform the action.  Wait on the result.
                await action(now, cancellationToken).
                    // Doing this here because synchronization context more than
                    // likely *doesn't* need to be captured for the continuation
                    // here.  As a matter of fact, that would be downright
                    // dangerous.
                    ConfigureAwait(false);

                // Wait.
                await Task.Delay(TimeSpan.FromSeconds(1), cancellationToken).
                    // Same as above.
                    ConfigureAwait(false);

                // Post the action back to the block.
                block.Post(DateTimeOffset.Now);
            }, new ExecutionDataflowBlockOptions
            {
                CancellationToken = cancellationToken
            });

            // Return the block.
            return block;
        }
    }
}
