﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ConsoleAppTaskRun
{
    public class Servico : NeverEndingTask
    {
        protected override void ExecutionCore(CancellationToken cancellationToken)
        {
            Random rdn = new Random();
            Console.WriteLine("Teste " + rdn.Next());
        }
    }
}
