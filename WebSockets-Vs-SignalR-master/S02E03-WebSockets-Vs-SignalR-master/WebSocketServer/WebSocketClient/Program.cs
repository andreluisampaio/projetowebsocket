﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WebSocketClient
{
    class Program
    {
        //https://www.demo2s.com/csharp/csharp-clientwebsocket-clientwebsocket.html

        private const string Address = "http://localhost:5000";

        static void Main(string[] args)
        {
            //WebRequestHandler handler = new WebRequestHandler();
            //handler.ServerCertificateValidationCallback = (_, __, ___, ____) => true;
            //handler.Credentials = new NetworkCredential(@"redmond\chrross", "passwird");
            //HttpClient client = new HttpClient(handler);
            while (true)
            {
                Console.WriteLine("Press any key to send request");
                Console.ReadKey();
                //var result = client.GetAsync(Address).Result;

                RunWebSocketClient();                
                //Console.WriteLine(result);
            }
        }

        public static async Task RunWebSocketClient()
        {
            ClientWebSocket websocket = new ClientWebSocket();
            string url = "ws://localhost:5000/";
            Console.WriteLine("Connecting to: " + url);
            await websocket.ConnectAsync(new Uri(url), CancellationToken.None);

            string connID = string.Empty;
            byte[] incomingData = new byte[1024];
            WebSocketReceiveResult result = await websocket.ReceiveAsync(new ArraySegment<byte>(incomingData), CancellationToken.None);
            if (result.CloseStatus.HasValue)
            {
                Console.WriteLine("Closed; Status: " + result.CloseStatus + ", " + result.CloseStatusDescription);
            }
            else
            {
                string receivedMessage = Encoding.UTF8.GetString(incomingData, 0, result.Count);
                Console.WriteLine("Received message: " + receivedMessage);
                connID = receivedMessage.Substring(8);
            }

            //string message = @"{ ""From"":""87b2a125-0ae6-45c9-962a-d155c688b54b"",""To"":"""",""Message"":""Hello""}";

            var obj = new MessageObject() 
            {
                From = connID,
                To = string.Empty,
                Message = "Hello"
            };

            string message = JsonConvert.SerializeObject(obj);

            Console.WriteLine("Sending message: " + message);
            byte[] messageBytes = Encoding.UTF8.GetBytes(message);
            await websocket.SendAsync(new ArraySegment<byte>(messageBytes), WebSocketMessageType.Text, true, CancellationToken.None);           
        }

        public static string constructJSONPayload(string connID, string recipients, string message)
        {            
            var json = string.Format(
                @"""From"": ""{0}"", 
                ""To"": ""{1}"",
                ""Message"": ""{2}""", connID.Substring(8, connID.Length), recipients, message);

            return JsonConvert.ToString(json);
        }

    }

    public class MessageObject
    {
        public string From { get; set; }
        public string To { get; set; }

        public string Message { get; set; }
    }
}
