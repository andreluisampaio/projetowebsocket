﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ServiceStack.Redis;
using System.Threading;
using System.Threading.Tasks;

namespace APIListenerRedis.Services
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
			string channelName = "Canal";

			using (var redisConsumer = new RedisClient("127.0.0.1:6379"))
			using (var subscription = redisConsumer.CreateSubscription())
			{
				subscription.OnSubscribe = channel =>
				{
					_logger.LogInformation("Subscribed to '{0}'", channel);
				};
				subscription.OnUnSubscribe = channel =>
				{
					_logger.LogInformation("UnSubscribed from '{0}'", channel);
				};
				subscription.OnMessage = (channel, msg) =>
				{
					_logger.LogInformation("Received '{0}' from channel '{1}'", msg, channel);
				};

				_logger.LogInformation("Started Listening On '{0}'", channelName);
                Task task = Task.Factory.StartNew((a) =>
                {
                    stoppingToken.ThrowIfCancellationRequested();
                    subscription.SubscribeToChannels(channelName);

                }, stoppingToken, TaskCreationOptions.LongRunning);
            }
        }
    }
}
