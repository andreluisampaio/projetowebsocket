﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Notificacao.Hubs;
using System.Text;
using System.Threading.Tasks;

namespace Notificacao.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NotificarController : ControllerBase
    {
        private readonly IHubContext<NotificacaoHub> _streaming;

        private readonly ILogger<NotificarController> _logger;

        public NotificarController(ILogger<NotificarController> logger, IHubContext<NotificacaoHub> streaming)
        {
            _logger = logger;
            _streaming = streaming;
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] WeatherForecast weather)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await WriteOnStream(weather);

            await WriteOnStream2(weather);

            return Ok(weather);
        }

        private async Task WriteOnStream(WeatherForecast weather)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Data: " + weather.Date.ToString("yyyy-MM-dd") + " | ");
            sb.Append("Temperatura: " + weather.TemperatureC.ToString() + " | ");
            sb.Append("Resumo: " + weather.Summary.ToString());

            await _streaming.Clients.All.SendAsync("addMessage", sb.ToString());
        }

        private async Task WriteOnStream2(WeatherForecast weather)
        {
            await _streaming.Clients.All.SendAsync("addMessage2", weather);
        }
    }
}
